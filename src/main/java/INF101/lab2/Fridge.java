package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    int max_size = 20;
    int ItemsInFridge = 0;
    List<FridgeItem> fridgeItems = new ArrayList<FridgeItem>();
    
    
    

    @Override
    public int nItemsInFridge() {
        
        return fridgeItems.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            fridgeItems.add(item);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeItems.contains(item)) {
            fridgeItems.remove(item);
        }
       
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        fridgeItems.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        List<FridgeItem> itemsToRemove = new ArrayList<FridgeItem>();
        for (FridgeItem i: fridgeItems) {
            if (i.hasExpired()) {
                itemsToRemove.add(i);
                expiredItems.add(i);    
            }
            
        }
        fridgeItems.removeAll(itemsToRemove);
        return expiredItems;
    }
    
}
